# LoanRepaymentCalculator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

### Node.js

At First you have to have Node.js version 10.9.0 or later.
To check your version, run node -v in a terminal/console window.
To get Node.js, go to [nodejs.org](https://nodejs.org/en/)

### npm package manager

Angular, the Angular CLI, and Angular apps depend on features and functionality provided by libraries that are available as npm packages. To download and install npm packages, you must have an npm package manager.
This setup guide uses the npm client command line interface, which is installed with Node.js by default.
To check that you have the npm client installed, run npm -v in a terminal/console window.

### Dependency Installation

After Cloning this project, go to the project directory and run `npm install` to download all the dependencies this project have.

### Development server

Run `ng serve -o` for a dev server. It will automatically open project in default browser. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
