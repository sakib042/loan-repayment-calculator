// General Modules
import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
// Routing Module
import {AppRoutingModule} from "./app-routing.module";
// Main Component
import {AppComponent} from "./app.component";
// Components
import {HomeComponent} from "./components/home/home.component";
import {HeaderComponent} from "./components/header/header.component";
import {FooterComponent} from "./components/footer/footer.component";

@NgModule({
  declarations: [
    AppComponent, HomeComponent, HeaderComponent, FooterComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, CommonModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
