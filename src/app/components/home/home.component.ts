import {Component, OnInit} from "@angular/core";

@Component({selector: "app-home", templateUrl: "./home.component.html", styleUrls: ["./home.component.scss"]})
export class HomeComponent implements OnInit {
  // Variables
  monthlyPayment = "0";
  totalLoanAmount = 0;
  totalInterestAmount = "0";
  totalToBePaid = "0";
  estimatePayOffDate = "DD/MM/YYYY";
  selectedDate = 1;
  selectedCurrency = 1;
  numberOfInstallment = "0";
  amountForEachInstallment = "0";
  interrestOfEachInstallment = "0";
  loanTakenForDays = 30;
  allInstallmentDateList = [];
  repaymentDateChanged = false;
  currencyList = [
    {
      value: 1,
      name: "Euro (&euro;)"
    }, {
      value: 2,
      name: "Pound (&#xa3;)"
    }, {
      value: 3,
      name: "Doller (&#x24;)"
    }
  ];
  repaymentDateList = [];

  constructor() {}

  // Currency Selection Function

  selectedCurrencyName(currency : number) {
    // console.log(currency);
    this.selectedCurrency = currency;
  }

  // Repayment Date Selection Function

  selectedRepaymentDate(date : number) {
    // console.log(date);
    this.selectedDate = date;
    this.repaymentDateChanged = false;
  }

  // Loan Priod Selection Function

  givenLoanPriod(months) {
    this.repaymentDateList = [
      {
        value: 1,
        name: "Every Day"
      }, {
        value: 7,
        name: "Every Week"
      }, {
        value: 15,
        name: "Every Fifteen Days"
      }, {
        value: 30,
        name: "Every Month"
      }
    ];

    this.loanTakenForDays = months.value * 30;

    //-- Check Loan Repayment Date Change with Loan Period Change

    if (this.loanTakenForDays % this.selectedDate != 0) {
      this.repaymentDateChanged = true;
    } else {
      this.repaymentDateChanged = false;
    }

    //-- Generate Valid Loan Repayment Date List with Loan Period Change

    if (this.loanTakenForDays % 60 == 0) {
      this.repaymentDateList.push({value: 60, name: "Every Two Months"});
    }
    if (this.loanTakenForDays % 90 == 0) {
      this.repaymentDateList.push({value: 90, name: "Every Three Months"});
    }
    if (this.loanTakenForDays % 180 == 0) {
      this.repaymentDateList.push({value: 180, name: "Every Six Months"});
    }
    if (this.loanTakenForDays % 360 == 0) {
      this.repaymentDateList.push({value: 360, name: "Every Twelve Months"});
    }
  }

  // Repayment Loan Calculation

  loanCalculation(data) {
    // console.log(data.value);

    let givenValue = data.value;
    this.totalLoanAmount = givenValue.loanAmount;

    var monthly = givenValue.interestRate / 12 / 100;
    var start = 1;
    var length = 1 + monthly;
    for (let i = 0; i < givenValue.loanPeriod; i++) {
      start = start * length;
    }
    let mp = Number(givenValue.loanAmount * monthly / (1 - 1 / start));
    this.monthlyPayment = mp.toFixed(2);
    var totrepay = Number(mp) * givenValue.loanPeriod;
    this.totalToBePaid = totrepay.toFixed(2);
    var totint = totrepay - givenValue.loanAmount;
    this.totalInterestAmount = totint.toFixed(2);

    let installmentNumber = this.loanTakenForDays / this.selectedDate;

    let eachInstallmentAmountWithInterest = totrepay / installmentNumber;
    this.amountForEachInstallment = eachInstallmentAmountWithInterest.toFixed(2);
    this.numberOfInstallment = installmentNumber.toFixed(0);
    let eachInstallmentAmountWithoutInterest = givenValue.loanAmount / installmentNumber;
    let eachInstallmentInterest = eachInstallmentAmountWithInterest - eachInstallmentAmountWithoutInterest;
    this.interrestOfEachInstallment = eachInstallmentInterest.toFixed(2);

    let date = new Date();
    let today = date.getTime();
    let newDate = this.selectedDate * 24 * 60 * 60 * 1000;

    let installmentDateList = [];

    for (let i = 0; i < installmentNumber; i++) {
      today = today + newDate;
      installmentDateList.push(today);
    }

    this.allInstallmentDateList = installmentDateList;

    // let installment = mp / 30 * this.selectedDate;
    // this.amountForEachInstallment = installment.toFixed(2);
    // let noi = totrepay / installment;
    // this.numberOfInstallment = noi.toFixed(0);
    // let interrestOfEachInstallment = totint / noi;
    // this.interrestOfEachInstallment = interrestOfEachInstallment.toFixed(2);
  }

  ngOnInit() {}
}
